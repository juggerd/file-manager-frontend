import {NgModule, OnInit} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TreeDirectoryComponent } from './tree-directory/tree-directory.component';
import { ShowcaseDirectoryComponent } from './showcase-directory/showcase-directory.component';
import { SearchDirectoryComponent } from './search-directory/search-directory.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { ShowcaseViewButtonsComponent } from './showcase-view-buttons/showcase-view-buttons.component';
import {Directory, DirectoryService} from "./directory.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    TreeDirectoryComponent,
    ShowcaseDirectoryComponent,
    SearchDirectoryComponent,
    BreadcrumbsComponent,
    ShowcaseViewButtonsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
