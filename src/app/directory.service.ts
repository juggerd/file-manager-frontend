import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export interface Directory {
  name: string;
  directory: string;
  flag: string; // <- may be need enum? only f and d exists ib result in this field
}

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private http_client: HttpClient) { }

  fetchDirectory(directory_name: string): Observable<Directory[]>  {
    return this.http_client.post<Directory[]>('http://127.0.0.1/api/show-directory',
      {'directory': directory_name}
    );
  }
}
