import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Directory, DirectoryService} from "../directory.service";

@Component({
  selector: 'app-tree-directory',
  templateUrl: './tree-directory.component.html',
  styleUrls: ['./tree-directory.component.scss']
})
export class TreeDirectoryComponent implements OnInit {
  @Input() tree_directory_data: Directory[] = [];
  @Output() onSelectedDirectory = new EventEmitter()

  constructor(private directoryService: DirectoryService) { }

  ngOnInit(): void {
  }
}
