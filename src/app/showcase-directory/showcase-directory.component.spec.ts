import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseDirectoryComponent } from './showcase-directory.component';

describe('ShowcaseDirectoryComponent', () => {
  let component: ShowcaseDirectoryComponent;
  let fixture: ComponentFixture<ShowcaseDirectoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowcaseDirectoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
