import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Directory} from "../directory.service";

@Component({
  selector: 'app-showcase-directory',
  templateUrl: './showcase-directory.component.html',
  styleUrls: ['./showcase-directory.component.scss']
})
export class ShowcaseDirectoryComponent implements OnInit {
  @Input() selected_directory_data: Directory[] = [];
  @Output() onSelectedDirectory = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

}
