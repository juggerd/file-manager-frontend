import {Component, EventEmitter} from '@angular/core';
import {Directory, DirectoryService} from "./directory.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selected_directory_name: string = '/';

  tree_directory_data: Directory[] = [];
  selected_directory_data: Directory[] = [];

  constructor(private directoryService: DirectoryService) {
  }

  ngOnInit(): void {
    this.directoryService.fetchDirectory(this.selected_directory_name).subscribe(response => {
      this.tree_directory_data = response;
    });
  }

  onSelectedDirectoryFromBreadcrumbs(directory_name: string) {
    this.directoryService.fetchDirectory(directory_name).subscribe(response => {
      this.selected_directory_name = directory_name;
      this.tree_directory_data = response;
    });
  }

  onSelectedDirectory(item: Directory) {
    let directory_name = `/${item.name}`;
    if (item.directory != '/') {
      directory_name = `${item.directory}/${item.name}`;
    }
    this.selected_directory_name = directory_name;
    this.directoryService.fetchDirectory(directory_name).subscribe(response => {
      this.selected_directory_data = response;
    });
  }
}
