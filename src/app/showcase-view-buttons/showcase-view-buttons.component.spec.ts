import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseViewButtonsComponent } from './showcase-view-buttons.component';

describe('ShowcaseViewButtonsComponent', () => {
  let component: ShowcaseViewButtonsComponent;
  let fixture: ComponentFixture<ShowcaseViewButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowcaseViewButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseViewButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
