import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input() selected_directory_name: string = '/';
  @Output() onSelectedDirectoryFromBreadcrumbs = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  getDirectories() {
    let path = this.selected_directory_name.split('/');
    let result = [];
    const n = path.length - 1;
    for (let i = 0; i < n; ++i) {
      result.push(path.join('/'));
      path.splice(-1,1);
    }
    result[result.length - 1] = '/';
    result.reverse();
    return result;
  }

}
